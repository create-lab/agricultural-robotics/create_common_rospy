#! /usr/bin/env python3

from typing import Any
import rospy

class SimpleSubscriber:
    def __init__(self, topic: str, message_type: Any) -> None:
        self.message = message_type()
        self.subscriber = rospy.Subscriber(topic, message_type, self.callback, queue_size=1)

    def callback(self, message: Any) -> None:
        self.message = message